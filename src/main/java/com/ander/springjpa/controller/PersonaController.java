package com.ander.springjpa.controller;
import com.ander.springjpa.model.Persona;
import com.ander.springjpa.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api")
public class PersonaController {

    @Autowired
    private IPersonaService iPersonaService;
    @GetMapping("/persona")
    public List<Persona> index() {
        return iPersonaService.findAll();
    }

    @GetMapping("/Persona/{id}")
    public Persona show(@PathVariable Integer id) {
        return this.iPersonaService.findById(id);
    }

    @PostMapping("/clientes")
    @ResponseStatus(HttpStatus.CREATED)
    public Persona create(@RequestBody Persona persona) {
        this.iPersonaService.save(persona);
        return persona;
    }

    @PutMapping("/clientes/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Persona update(@RequestBody Persona persona, @PathVariable Integer id) {
        Persona currentPerson = this.iPersonaService.findById(id);
//        currentPerson.setIdPerson(persona.getIdPerson());
        currentPerson.setName(persona.getName());

        return currentPerson;
    }

    @DeleteMapping("/clientes/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        Persona currentPerson = this.iPersonaService.findById(id);
        this.iPersonaService.delete(currentPerson);
    }


}
