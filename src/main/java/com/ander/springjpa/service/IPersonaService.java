package com.ander.springjpa.service;

import com.ander.springjpa.model.Persona;
import java.util.List;

public interface IPersonaService {

    public List<Persona> findAll();
    public void save(Persona persona);
    public Persona findById(Integer id);
    public void delete(Persona persona);

}
