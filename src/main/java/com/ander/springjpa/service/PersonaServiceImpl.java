package com.ander.springjpa.service;

import com.ander.springjpa.model.Persona;
import com.ander.springjpa.repo.IPersonaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements IPersonaService {


    @Autowired
    private IPersonaRepo iPersonaRepo;


    @Override
    public List<Persona> findAll() {
        return (List<Persona>) iPersonaRepo.findAll();
    }

    @Override
    public void save(Persona persona) {
        iPersonaRepo.save(persona);
    }

    @Override
    public Persona findById(Integer id) {
        return iPersonaRepo.findById(id).orElse(null);
    }

    @Override
    public void delete(Persona persona) {
        iPersonaRepo.delete(persona);
    }
}
