package com.ander.springjpa.repo;

import com.ander.springjpa.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {


}
